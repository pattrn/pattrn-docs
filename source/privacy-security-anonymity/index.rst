.. _security:

============================
Privacy, security, anonymity
============================

Use Pattrn only with public data
--------------------------------

Pattrn is a public platform. In the current version of Pattrn, all data
featured in a Pattrn app or in a ``PATTRN_Master`` spreadsheet is by
definition publicly available online to anyone.

For this reason, **only data safe to be published online must be
included in a Pattrn app.**

This applies to:

* data entered by Editors directly into the ``PATTRN_Master`` spreadsheet
* data entered by Editors into the ``PATTRN_Master`` spreadsheet via the
  Pattrn Editor
* data submitted by Observers of a Pattrn app to its Editors, via the data
  contribution tool integrated in the Pattrn app.

By "data safe to be published online" we mean:

* data that will not put the security of anyone at risk (Editor,
  Observer, or Third Party referred to directly or indirectly in the
  content of the data)
* data that does not disclose the identity of anyone (Editor, Observer,
  or Third Party referred to directly or indirectly in the content of
  the data) without the explicit consent of the individual(s)
  concerned.
* data that does not violate any of the Data Protection laws and
  policies in place in any of the countries where the data is processed
  (collected, submitted, reviewed, stored, or published)

**The responsibility of the correct and safe use of Pattrn is with its
users.**

Anonymous contributions
-----------------------

The Pattrn app enables users to contribute data to a given
instance, by means of "anonymous contributions" (see :ref:`submitting-new-data`).

By "anonymous contributions", we mean that the technology used in Pattrn
won't track nor store any data about the identity, IP addresses, or location of
the individual submitting data to the Editors of a Pattrn app.
Consequently, the Editors of the Pattrn app will not have access,
through Pattrn, to any data about the identity, IP addresses, or location of the
individual(s) submitting data if such individuals do not leave any
contact information in the optional field included in the data
submission form.

Nonetheless, it is important to note that:

* The anonymity of such contributions is only as secure as the
  technology used in Pattrn – in this specific case, Google technology
  underpinning the transfer of digital information from and to Google
  Apps. While the choice of this technology for the development of Pattrn
  was in part motivated by the well-documented stability and security of
  Google Apps, every technological system has its vulnerabilities. The
  Pattrn Project **cannot guarantee** that all information about the
  online identity, IP addresses, or location of an individual submitting data
  anonymously through Pattrn, is secure against any form of digital
  attack or surveillance operation.

* Regardless of the specific technology used for the transfer of data
  through Pattrn: government agencies, Internet Service Providers, or the
  organisation administrating the network from which an anonymous data
  submission is performed, are likely to be able to access details about
  the identity, IP addresses, and location of the individual(s) submitting data.

For this reason **anonymous contributions of data should not contain
any information that, should the identity of the individual submitting
this data be disclosed, could put the security of this individual at
risk.**

This point is clearly and explicitly included in the **information and
consent form** that any individual is required to approve in order to
submit data anonymously via Pattrn.

In addition, **it is the responsibility of Editors** reviewing
contributions of data to make sure that all data received via anonymous
contributions **is safe to be published** before actually publishing it
on a Pattrn app.