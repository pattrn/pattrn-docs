============
About Pattrn
============

What is Pattrn?
---------------

**Pattrn** is a tool to map complex events—such as conflicts,
protests, or crises—as they unfold.

Working as an **aggregator of data** in different media formats as well
as an advanced **data visualisation** platform, Pattrn enables its
community of users to share and collate first-hand reports of events on
the ground and to **make sense** of diffused fragments of information.

Its principle is simple: everything that happens does so at a given
place and time. The tool enables its users to build a **dataset of
events** with space and time coordinates, and to add **tags** ,
**media** , and **content** to these events. Anyone can contribute data,
anonymously.

The database can then be explored through an online **visualisation
platform** : while a map provides access to the **details** of each
event, interactive charts and filters enable to reveal **patterns**
across the data. Together, users of Pattrn can thereby create the **big
picture** of an ongoing situation.

Designed to be used in the fields of **conflict monitoring** , **human
rights** , **investigative journalism** , **citizen science** , and
**research** at large, Pattrn responds to new ways of reporting from the
front in the digital age.

How does Pattrn work?
---------------------

Components
..........

A Pattrn instance comprises of:

1) A frontend visualisation platform (the **Pattrn app** )
   combining several JavaScript libraries – which pulls and visualises
   data from...

2) ...a **dataset** of **events**, each happening at a given time and
   location. The dataset linked to a Pattrn instance can either be
   published with the Pattrn app, or it can be hosted in the cloud,
   where it can be updated as needed (for example, by adding new
   events as they happen), with updates reflected immediately
   in the Pattrn app.

When hosting a dataset in the cloud, Pattrn editors can choose whether
and how to allow contributors to submit new events: the Pattrn app
simply retrieves the latest data available from the configured dataset,
which itself can be growing dynamically as contributors submit new
data or modify existing data.

In order to provide a convenient and simple to use interface for
crowdsourcing of event data, Pattrn includes a proof-of-concept
**Pattrn Editor** for users who host the Pattrn dataset on
Google Sheets: this data editing web application is built on Google
Apps Script and facilitates the process of entering and editing data.

The Pattrn Editor is aimed at users who wish to set up a Pattrn
instance with crowdsourced data quickly and easily. Advanced
users, or users who wish to avoid using Google's proprietary and
opaque infrastructure for reasons of privacy and accountability will
likely want to use ad-hoc data collection and verification workflows:
as long as the dataset can be accessed via HTTPS in GeoJSON format,
the Pattrn app can be configured to visualise its data.

Editors/Observers
.................

Individuals or organisations that start a Pattrn instance are called
**Editors**.

Editors can:

* Configure how data is visualised on the Pattrn app
* Edit the settings of the Pattrn app

When using Google Sheets for the backend dataset and the Pattrn
Editor to edit data, Editors can also:

*  Access the Pattrn Editor linked to the Pattrn app
*  Review, verify, and publish external contributions of data on the
   Pattrn app

General online users of a Pattrn instance are called **Observers**. In
addition to exploring the data available on the Pattrn app,
Observers can participate to the research by contributing new data,
either through ad-hoc data reporting/editing workflows put in place
by Pattrn Editors, or through the Pattrn Editor interface mentioned
above.

Why Pattrn?
-----------

We are witnessing a revolution in the way people access information
about events.

With the global spread of digital connectivity, conflicts, protests, or
crises around the world are increasingly reported by the very people
that experience them first-hand. However, it is more and more
challenging **to make sense of the mass of data** created, to piece
together seemingly disparate events, and to distinguish between facts
and rumours.

The Pattrn project set out to respond to the challenges and
opportunities of this new media landscape.

Pattrn is primarily developed as a tool to support research and
information around armed conflicts, human rights violations, or social
and environmental crises. In addition to working as a crisis-mapping
tool, Pattrn integrates powerful analytics that allow for temporal and
spatial trends, or **patterns** , to be revealed across large datasets.
