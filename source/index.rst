======
Pattrn
======

Data-driven, participatory fact mapping for:

* conflict monitoring
* investigative journalism
* human rights
* citizen science
* research and analysis

Pattrn is free and open source software, distributed under the
`GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.en.html>`_
version 3 or any later version.

This user guide is free and open source as well, distributed under the
`GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.en.html>`_
version 3 or any later version.

.. toctree::
   :caption: Table of contents

   about-pattrn/index.rst
   getting-started/index.rst
   how-to-use-pattrn/index.rst
   managing-data/index.rst
   pattrn-data-packages/index.rst
   pattrn-on-gapps/index.rst
   the-pattrn-editor/index.rst
   contributing/index.rst
   troubleshooting/index.rst
   getting-help/index.rst
   privacy-security-anonymity/index.rst

Authors and contributors
------------------------

The Pattrn project was initiated thanks to a Proof-of-Concept Grant from the
`European Research Council <https://erc.europa.eu/>`_, awarded to
Prof. Eyal Weizman, in the framework of
`Forensic Architecture <http://forensic-architecture.org>`_ (2014-2015).

Pattrn v2.0
...........

* Technical lead and data science: `andrea rota <https://github.com/hotzeplotz>`_
* Project management: `Nick Axel <https://github.com/alucidwake>`_

Pattrn v1.0
...........

* Project Architect: `FSBRG <https://twitter.com/fsbrg>`_ (Francesco Sebregondi)
* Frontend development: `TEKJA Data <http://tekja.com/>`_
* Pattrn editor development: `Digital Consolidation <http://www.digital-consolidation.co.uk/>`_

*All new contributors to the Pattrn project will be duly credited.*

License
-------

Pattrn is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Pattrn is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Pattrn.  If not, see <http://www.gnu.org/licenses/>.