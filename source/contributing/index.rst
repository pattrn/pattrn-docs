=========================================
Contributing to the development of Pattrn
=========================================

As a free software project, Pattrn welcomes and encourages contributions
to its development by the community of its users.

The code of Pattrn is hosted on GitHub, which also provides the
framework for collaborative development of the project.

There are many ways people can contribute to the development of Pattrn.
Below is a list of the most common ones.

.. _pattrn-gitter-room:

Joining the Pattrn developers discussion room
---------------------------------------------

Pattrn developers and users hang out on the `Pattrn discussion room
on Gitter <https://gitter.im/pattrn/community>`_. Access is open
to anyone: please join the discussion if you have any questions about
using or improving Pattrn or about developing new features.

Writing code
------------

Developers wishing to contribute code to the development of Pattrn can
do so by following the standard GitHub procedures of open source
development, such as `forking the Pattrn
repository <https://help.github.com/articles/fork-a-repo/>`_, `creating
a pull
request <https://help.github.com/articles/using-pull-requests/>`_, etc.

For more information, visit `Github's help
page <https://help.github.com/>`_.

Reporting bugs and issues
-------------------------

Reporting issues is an essential part of the development process.

Anyone can raise an issue. Whether you are a developer or a simple user
of Pattrn, whether it is about a bug of the application or related to
the User Interface, raising an issue is always welcome and can really
help improving Pattrn.

The best way to raise an issue is to do so directly on GitHub. To do so,
please refer to the `simple guide
here <https://help.github.com/articles/creating-an-issue/>`__

Alternatively, feel free to drop an email to support@pattrn.co with a
description of the issue you wish to raise.

Writing/Editing documentation
-----------------------------

The manual you are reading is developed in its own GitLab repository:
you can help us improve the present documentation of Pattrn by
forking the `documentation repository <https://gitlab.com/pattrn/pattrn-docs>`_
and submitting a merge request.

Sharing ideas and spreading the word
------------------------------------

Ideas and suggestions to improve Pattrn, technical or not in nature, are
very welcome. Please share them by sending an email to
support@pattrn.co.

Help us grow an active community of users/developers by spreading the
word about Pattrn in your social and professional networks.

Financial support
-----------------

Fully non-profit, the Pattrn Project depends on funding and financial
support in order to maintain and develop Pattrn.

We welcome donations and funding from organisations interested in using
Pattrn for their work, or from foundations dedicated to support
initiatives in related fields such as open-source, open data, human
rights, information transparency, or citizen journalism.

Please get in touch by sending an email to support@pattrn.co.

Governance structure
--------------------

Pattrn is maintained and governed by the Pattrn Project.

The Pattrn Project is a unit hosted at Goldsmiths, University of London,
in the Department of Visual Cultures.

As of December 2015, it is co-directed by Eyal Weizman, Director of
Forensic Architecture, and Francesco Sebregondi, Project Architect of
Pattrn.

Decisions regarding the development trajectory and roadmap of the Pattrn
project are taken in consultation with the Pattrn Advisory Board,
composed of developers and expert practitioners in the fields of use of
Pattrn.

As Pattrn grows and starts involving an active community of users and
developers, mechanisms for this community to take an active part in the
decision-making process will be put in place. For comments or
suggestions, please send us an email at support@pattrn.co.
