.. _getting-help:

================
Help with Pattrn
================

Join the Pattrn developers discussion room
------------------------------------------

Pattrn developers and users hang out on the `Pattrn discussion room
on Gitter <https://gitter.im/pattrn/community>`_. Access is open
to anyone: please join the discussion if you have any questions about
using or improving Pattrn or about developing new features.

Google Group
------------

Pattrn has a Google Group to facilitate discussions and support among
its community of users, `accessible
here <https://groups.google.com/forum/#!forum/pattrn>`_

The group is public, and anyone can join. Just open the URL above while
logged in to your Google Account, and click on "Join the Group".

Once you've joined, you'll be able to create new topics and post
responses from the Pattrn Google Group online page, or directly from
your Gmail address, by sending an email to pattrn@googlegroups.com

Get in touch via email or Twitter
---------------------------------

While sharing your question or issue on the Pattrn Google Group is the
recommended way to get help, you can also contact us with ideas,
suggestions, or questions by:

* sending us an email at support@pattrn.co

* tweeting us on `@pattrn\_ <http://twitter.com/pattrn\_>`_

We'll do our best to get back to you promptly.
