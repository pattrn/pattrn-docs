.. _troubleshooting:

===============
Troubleshooting
===============

Pattrn isn't loading any data when using the Google Sheets backend
------------------------------------------------------------------

If, after setting it up by following the step-by-step guide (section 2
of the doc) and having entered a dataset into the ``Pattrn_Master``
spreadsheet, your Pattrn Platform doesn't load any data, please check
that:

* Every row in the ``Pattrn_Master`` spreadsheet that contains data in
  any of its cells, also contains valid data in the cells corresponding to
  the ``latitude``, ``longitude`` and ``date_time`` columns

In particular, check that all rows supposed to be empty are indeed
completely empty of all data.

Try reloading the Pattrn app. If it still doesn't load data,
please check that:

* All the data in the ``latitude``, ``longitude`` and ``date_time``
  columns is formatted correctly

To help with the process, the ``Pattrn_Master`` spreadsheet integrates data
validation formulas for each column. Any cell wrongly formatted in the
``latitude``, ``longitude`` or ``date_time` columns will be marked with an
orange triangle in the top right corner or the cell. Make sure you
correct the formatting of the data.

Try reloading the Pattrn app. If it still doesn't load data,
please check that:

* The public URL of the ``Pattrn_Master`` spreadsheet matches the one
  configured in the ``config.json`` file of your Pattrn instance

You can check the URL of the ``Pattrn_Master`` spreadsheet by opening it
from your Google Drive and clicking on ``File > Publish to the web...``.
The URL displayed needs to match the one inputted in the ``config.json``
file, in the ``public_spreadsheet`` field.

Save the file, re-upload it to your server, and try reloading the Pattrn
app. If it still doesn't load data, please check that:

* There are no formatting errors in the ``config.json`` file of your
  Pattrn instance

In particular, check that all the quotation marks in the ``config.json``
file are straight, double quotation marks. Visual text editors may
automatically replace straight quotation marks with curly ones: check
the advice at the beginning of the :ref:`getting-started` tutorial about
text editors.

Save the file, re-upload it to your server, and try reloading the Pattrn
app. If it still doesn't load data, please see the :ref:`getting-help`
section.