=================
How to use Pattrn
=================

Introduction
------------

The Pattrn app is an **interactive data dashboard** enabling its
users to explore, visualise, and query the dataset of events linked
to the app (see the section of this manual on *Preparing your data for
Pattrn*).

Its interface allows users to move across scales of analysis: it gives
access to the **granular details** of each singular event, and helps
revealing **patterns** across diffuse data.

This section of the Pattrn manual provides an overview of the key
analytical functions of the Pattrn app.

*Note:* we advise to use the latest version of either the
`Firefox <https://www.mozilla.org/firefox/new/>`_,
`Chromium <https://download-chromium.appspot.com/>`_ or
`Chrome <https://www.google.com/chrome/>`_ browsers.

The event map
-------------

.. add link to base layer documentation

The central component of the Pattrn interface is the map area. You can
zoom in and out by using the + / - buttons, and change the base layer to
display a choice of base map layers.

On the map, the black circles containing numbers are **clusters of
events.** The number corresponds to the number of events registered in
that location. Clicking on these black circles zooms into the cluster
and decomposes it into smaller clusters or **individual events** (coloured
dots with no numbers on them).

Each individual event can be clicked in order to access all the
**details** about it.

When several events have the exact same coordinates, a **spiral** of
coloured circles will appear around the center point, allowing users to
select each event separately.

Event details
-------------

When clicking on an individual event's marker on the map, the **Event
details** panel slides in from the right, revealing all the event's data,
split into tabs.

The first tab is the **Summary** tab: it displays the available textual
report for the selected event.

At the bottom, a summary table lists the key data about the event.

The other tabs – **Photos** , **Videos** , **Links** – provide access to
all other available media and content pertaining to this event. If no
photos, videos or links are available for this event, their corresponding
tabs will not be displayed.

By clicking on the X icon at the top left of the Event details panel,
or anywhere on the map, the panel is folded back out of view, allowing
users to focus on the map and on the charts below it.

Charts and filters area
-----------------------

General information about the interactive charts
................................................

In the lower part of the Pattrn interface, you will find a series of advanced
charting and filtering tools.

When loading the Pattrn app in the browser, the list of available
charts is presented in the left-side panel, which is expanded by default,
pushing the map to the right. By clicking anywhere on the map or charts,
the **Chart menu** slides back off the screen. To make the Chart menu
slide back in again at any time, click on the + sign to the top left of
the map area.

By selecting a chart from the **Chart Menu** , users can plot data
**over time** , visualise it **by type** (tag) or by **Yes/No**
questions.

All the charts are **interactive** : they enable users not only to
**visualise** data, but also to **filter** it according to certain
criteria. By **combining filters** , users can quickly navigate a large
dataset, **reveal patterns across the data** , and make sense of complex
situations.

The **map** itself works as a **filtering device**. Only the events
contained in the frame of the map are visualised and accounted for in
the Charts and Filters area. This means that, if users want to focus on the
events that took place in a particular area, they can zoom and center
the map around that area: only the data pertaining to events in that
area will be charted in the Charts and Filter area.

At the bottom of the map, users can read the **number of
events** contained in the specific frame of the map, out of the full
number of events in the dataset.

Charting and filtering over time
................................

Selecting a chart **over time** displays a Time Chart, whose timespan
corresponds to that between the oldest and the most recent event in the
dataset of the Pattrn instance.

Different Time Charts are available. By default, all instances will
feature a chart of the number of events over time.
In addition, for each column of numeric data entered in the dataset, a
time chart will be **automatically generated** by the Pattrn app,
plotting that specific variable over time.

By **clicking and dragging** a window across a Time Chart, users can
select a specific period of time as a filter. When users click and drag
a window of time in a Time Chart, all the events that did not take place
during that specific window of time will be filtered out of the map.
This feature enables users to focus on any specific period of time.

Charting and filtering by type
..............................

Selecting a chart **by type** displays a Bar Chart: each bar corresponds
to a given tag, and its height corresponds to the number of events that
have been assigned this tag.

For each column of tag data entered in the dataset, a Bar Chart will be
**automatically generated** by the Pattrn app, charting the
distribution of tags in that column across all events visible in the
active map frame.

By **clicking on a bar** in a Bar Chart, users can select a specific tag
as a filter. When a bar is clicked, all the events that have not been
assigned the corresponding tag will be filtered out of the map. This
feature enables users to focus on specific characteristics of the events
in the dataset.

Charting and filtering by Yes/No questions
..........................................

Similar to filtering by type, selecting a chart **by Yes/No questions**
displays a Yes/No Bar Chart: each bar corresponds to one of the three
possible answers (Yes, No, Unknown) to the selected question, and its
height corresponds to the number of events that match this answer.

For each column of Yes/No data entered in the dataset, a Yes/No Bar
Chart will be **automatically generated** by the Pattrn app,
charting the distribution of values in that column across all events
visible in the active map frame.

By **clicking on a bar** in a Yes/No Bar Chart, users can select a
specific answer as a filter. When a bar is clicked, all the events that
don't match that answer will be filtered out of the map. This feature
enables users to quickly isolate events in the dataset that fall into a
given category.

Checking/Resetting filters
..........................

When a filter is on, the **Funnel icon** located top right of the Charts
and Filters area turns black. Users can thereby quickly check whether they
are looking at the whole dataset, or at a filtered fraction of it.

To reset all filters and reload the page, click on the **Reload icon**
located top right of the Charts and Filters area.

Search field
............

The search field enables users to undertake **dynamic keyword searches**
across the entire dataset. As soon as a few characters are typed in the
search field, all events that do not contain this specific string of
characters anywhere in their row of data are filtered out of the map.

Sharing a Platform
..................

*Note: Only available if the Sharing Buttons have been set up* (see
:ref:`sharing-buttons`).

Whether you are an Editor or an Observer, you can easily share a link to
the app on social media channels, by using the integrated sharing
tools.

On the header of the Pattrn app, click on **SHARE**. A range of
familiar icons appear that will facilitate the process of sharing a link
to the app on your Twitter, Facebook, or Google Plus account,
etc.

.. _submitting-new-data:

Submitting New Data and Edits
.............................

The Pattrn app enables Observers not only to explore the data
available on the Platform, but also to **contribute new data for review
by Editors** of the app, when this is set up to use Google Sheets
as data source with the Pattrn Editor enabled.

To do so, Observers can click on the Edit/Add Event link top right of
the Event Details column. They can then chose between **Add a new
Event** or, if an event was pre-selected, **Edit this Event.**

Clicking on **Add a new Event** opens up an interface similar to that of
the Pattrn Editor, through which Observers can enter data about the
event they intend to report.

Once the data is entered–which can include photos, embedded videos, or
links–Observers can submit the new event.

**Edit this Event** opens up a similar interface, by which the Observer
can edit the data already available about an event–both adding data
and correcting the information currently displayed on the Platform–and
submit the edits.

In both cases, by default, **the submission process is anonymous** , but
Observers can choose to leave their contact, should they accept to be
contacted by Editors who would wish to contact the source of the data
contributed.

**Warning:** *for details about this feature, see* :ref:`security`.
