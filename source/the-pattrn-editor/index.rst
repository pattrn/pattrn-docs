.. _pattrn-editor:

============================
How to use the Pattrn Editor
============================

Accessing the Pattrn Editor
---------------------------

In order to access the Pattrn Editor, open the ``PATTRN_Admin`` spreadsheet
in your Google Drive.

If you have followed the process detailed in the :ref:`pattrn-on-gapps` section,
you should have already copied the URL of the Pattrn Editor
Script in the cell A2 of the ``PATTRN_Admin`` spreadsheet.

To access the Pattrn Editor as an Editor, open the link to be found in
cell A4, under ``script_url_as_editor``.

You can bookmark this URL for quicker access.

Once the URL is open in your browser, a login page will be displayed.
Enter your Editor **username** and **password**. You can find – or
create – your username and password in the ``PATTRN_Admin_Password``
spreadsheet (see :ref:`gapps-editors-setup`).

*Note: The recommended browser to use the Pattrn Editor is*
`Google Chrome <https://www.google.com/chrome/>`_.

Editing Tab
...........

Main Table view
~~~~~~~~~~~~~~~

Once you are logged in as an Editor, the first tab that will be
displayed by default is the Editing Tab.

At the centre of your screen is a table. This table mirrors the table in
the ``PATTRN_Master`` spreadsheet.

**Any edits to the data entered via the Pattrn Editor will be reflected
on the** ``PATTRN_Master`` **spreadsheet, and vice versa.**

The top part of the page will display:

* the title of your Pattrn app, as configured in the spreadsheet
  ``PATTRN_Admin``

* the number of events currently in the ``PATTRN_Master`` dataset

* a Live Event Search field, to access specific events via keywords.
  Note: you will need to press Enter after typing the keywords for
  the search function to be launched.

* a "Refresh" button, which calls for a refresh of the entire table.

Create / Delete columns
~~~~~~~~~~~~~~~~~~~~~~~

In order to customise your data structure, you can **create new
columns** in your dataset. Specifically, you can create:

* up to 5 new columns of **numeric data**

* up to 5 new columns of **data tags**

* up to 5 new columns of **Boolean data** (Yes/No)

To create a new column of data, click on the "Create" button in the top
right corner of the Main Table view. Select the type of data the column
will contain, and name the column. Note: you can only use lowercase
characters, and ``_`` (underscore) as a divider: no spaces, symbols
or uppercase characters are allowed.

To delete a column of data, click on the "Delete" button in the top
right corner of the Main Table view. Select the name of the column you
want to delete, click Delete. Note: there is no going back once it is
deleted, hence the two warning messages that require your OK.

Display of Events: by clusters, or all events
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the top left corner of the Main Table View, you will find a series of
blue buttons, such as "1-500" or "All".

In order to improve the responsiveness and performances of the Pattrn
Editor, by default the Main Table View will only load the first 500
events of a dataset. Depending on the volume of your dataset, a series
of blue buttons will be displayed, that enable you to breakdown you
entire dataset into cluster of 500 events, such as "1-500", "501-1000",
"1001-1500", etc...

Click on "All" to load all your events at once in the Main Table View.
Note that if you have a dataset of 2000+ events, and with each event
containing large volumes of data, the Pattrn Editor may be slow to
respond and could even crash, as it will have reached the limit of its
processing capacity.

Adding or editing an event
~~~~~~~~~~~~~~~~~~~~~~~~~~

To add an event to the dataset, click on the **Add New Event** button
at the bottom left corner of the Main Table View.

To edit an existing event, **double click the row** of the event in the
Main Table.

Both functions open the **Event Editing interface**.

Using the Event Editing interface will enable you to enter data about
the events you are logging in the dataset of the Pattrn Platform in a
**user-friendly** way, that will **automatically populate the
PATTRN_Master spreadsheet with data that is correctly formatted for the
Pattrn app.**

The Event Editing interface is designed to be self-explanatory. If you
have questions about its functioning and how to enter data, please see
:ref:`getting-help`.

Review of Contributions Tab
...........................

New contributions
~~~~~~~~~~~~~~~~~

*NOTE: This function is still a experimental.
Please do send us feedback about any issues you run into, or
improvements you would suggest.*

When Observers of your Pattrn app **contribute new data** through
the dedicated link in the Pattrn Platform (see :ref:`submitting-new-data`),
Editors can review the contributions through the Review of Contribution
Tab in the Pattrn Editor.

As Editors log in, if there are new contributions, the Pattrn Editor
will launch on the Review of Contributions Tab by default.

Select the contribution you would like to review and click "Review".

Review of a new event contribution
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If the contribution was submitted by an Observer as a new event,
clicking on "review" will directly open the Editing Interface, with the
data submitted by the Observer pre-loaded in the interface. Editors
should carefully review this data. They can edit any field so as to
correct any data after research and verification about the contributed
event. Once reviewed and verified, the Editor can click on the "Save"
button at the top of the page to include the contributed event in the
main dataset of the Pattrn app.

Review of an edited event contribution
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If the contribution was submitted by an Observer as an edited event,
clicking on "Review" will open an interface that displays the edits
suggested by the Observer to the data originally attached to an event in
the dataset.

Editors can accept or reject each edit, by clicking on the corresponding
button in a cursor with two positions. By default the Reject button is
displayed. Click in the grey area at his right to move the cursor right
and let the Accept button appear.

In order to help with the process of verification of the Edits proposed,
Editors can also load the data originally attached to the edited event
in a new window, by clicking on the blue link next to the Event ID.
Note: this is for consultation purposes only.

Once the Editors have selected which Edits they accept and which one
they reject, they can load the full data of the now edited event by
clicking on the "Load" button in the top left. This step is for a final
review of all the data attached to this specific event, merging
contribute data and original data, before saving this edited event into
the main dataset by clicking "Save". Editors can also make new edits at
this step.

The ``PATTRN_Audit`` spreadsheet
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

All contributions are logged in the ``PATTRN_Audit`` spreadsheet,
located in your Google Drive.

In the event of a wrong manipulation during the review process (valuable
edits discarded, review of contribution not saved, etc...), go to the
``PATTRN_Audit`` spreadsheet to consult a **log of all changes** to the
``PATTRN_Master`` spreadsheet, including changes suggested by Observers.

Contributions can be accessed in their original form in the
``PATTRN_Audit`` spreadsheet, and if needed, copied and pasted into the
``PATTRN_Master`` spreasheet (after review and confirmation).

The ``PATTRN_Audit`` spreadsheet is also where the **contact details** of
contributing Observers can be accessed (if the contribution was not
anonymous).

Collaboration among multiple Editors
....................................

Simultaneous work among multiple Editors
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Pattrn Editor can be accessed by multiple Editors at the same time,
so as to enable collaborative work on the dame dataset by multiple
Editors.

Note: The Pattrn Editor has been tested in conditions of up to five
Editors working together simultaneously, without any noticeable issue or
loss in performance.

Comments
~~~~~~~~

The COMMENTS field at the bottom of the Event Editing Interface is
useful to leave notes that won't display on the Pattrn app.

*Warning*: **Do not input any sensitive or personal data in the COMMENTS
field!** *As it is contained in the* ``PATTRN_Master`` *spreadsheet, which is
published online, Editors must assume that all data entered in COMMENTS
is virtually public: although not displayed in the Pattrn app, it can
be seen by expert users by looking at the data that is loaded by the
Pattrn app through the browser's Developer Tools.*

Drafts
~~~~~~

The Event Editing interfaces also enables to save an Event as a Draft,
for situations in which all data about an event has not yet been
entered. An Editor can then recover the draft, even when the draft was
created by another Editor, and resume the data entry process on that
draft.

To do so, from the main table view, click on "Add a new Event", then
click "Recover draft". If you are another Editor has previously saved an
Event as "draft", you will be able to select the draft and load the data
already inputted in the draft.

Once you've finished editing the Event, click "Save" to add the event to
the main dataset.
